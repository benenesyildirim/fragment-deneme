package com.example.enesyildirim.fragmentdeneme;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class frag2 extends Fragment implements View.OnClickListener {
    TextView email_et;
    Button send_btn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fra2,container,false);
        email_et = view.findViewById(R.id.email_et);
        send_btn = view.findViewById(R.id.send_btn);
        send_btn.setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View v)
    {
        if(v.getId() == send_btn.getId())
        {
            email_et.setText("Email işte yaa...");
        }
    }
}
