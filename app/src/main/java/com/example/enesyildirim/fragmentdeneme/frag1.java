package com.example.enesyildirim.fragmentdeneme;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class frag1 extends Fragment implements View.OnClickListener
{
    Button ok_btn,sifremiunuttum_btn;
    TextView name_et,pass_et;
    View view;
    FragmentTransaction ft;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.frag1,container,false);
        ok_btn = view.findViewById(R.id.ok_btn);
        sifremiunuttum_btn = view.findViewById(R.id.sifremiunuttum_btn);
        name_et = view.findViewById(R.id.name_et);
        pass_et = view.findViewById(R.id.pass_et);
        ok_btn.setOnClickListener(this);
        sifremiunuttum_btn.setOnClickListener(this);

        ft = getFragmentManager().beginTransaction();
        return view;
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == ok_btn.getId())
        {
            name_et.setText("Well Done!");
            pass_et.setText("Oldu Moruk!");
            ft.commit();
        }
        if(v.getId() == sifremiunuttum_btn.getId())
        {
            name_et.setText("Sifreni mi unuttun?!?!?!");
        }
    }
}
